% wildcard.sl
% This wrapper around the wildcard module can be autoloaded with
% autoload("wildcard_match", "wildcard");

import("wildcard", "wildcard");

%!%+
%\function{wildcard_match}
%\synopsis{Match a string against a glob pattern}
%\usage{Int wildcard_match(String_Type or String_Type[] str, String_Type pat)}
%\description
%  The \var{wildcard_match} function returns 1 if \var{str} matches the
%  glob pattern \var{pat}, otherwise it returns 0.  If \var{str} is an array,
%  the \var{wildcard_match} function will be applied to each element and the
%  result returned as an array.
%\seealso{string_match, glob_to_regexp, listdir}
%!%-
define wildcard_match()
{
   variable args = __pop_args(_NARGS);
   wildcard->wildcard_match(__push_args(args));
}
provide("wildcard");
