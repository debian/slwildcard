/* wildcard.c
 * Wildcard matching function.
 * 
 * $Id: wildcard.c,v 1.5 2008/06/16 18:09:41 paul Exp paul $
 * 
 * Copyright (c) 2004, 2008 Paul Boekholt.
 * Released under the terms of the GNU GPL (version 2 or later).
 * 
 * This is a simple wildcard matching function based on the one in Steve Baker's
 * 'tree' program.
 */

#include <slang.h>
SLANG_MODULE(wildcard);

#define MODULE_MAJOR_VERSION	0
#define MODULE_MINOR_VERSION	5
#define MODULE_PATCH_LEVEL	0
static char *Module_Version_String = "0.5.0";
#define MODULE_VERSION_NUMBER	\
   (MODULE_MAJOR_VERSION*10000+MODULE_MINOR_VERSION*100+MODULE_PATCH_LEVEL)


/*
 * Check pattern validity
 * returns:
 *    1 if valid
 *   -1 on a syntax error in the pattern
 */
static int patcheck(const char *pat)
{
   while(*pat)
     {
	switch(*pat)
	  {
	   case '[':
	     pat++;
	     
	     if(*pat == '^' || *pat == '!')
	       pat++;

	     while(*pat != ']')
	       {
		  switch(*pat)
		    {
		     case '\\':
		       pat++;
		       if (!*pat)
			 return -1;
		       break;
		     case 0:
		       return -1;
		     default:
		       if(pat[1] == '-')
			 {
			    pat += 2;
			    if(*pat == '\\')
			      pat++;
			    if(!*pat)
			      return -1;
			 }
		    }
		  pat++;
	       }
	     break;
	   case '\\':
	     pat++;
	     if(!*pat)
	       return -1;
	     break;
	   default:
	     pat++;
	  }
     }
   return 1;
}

/*
 * Patmatch() code courtesy of Thomas Moore
 * returns:
 *    1 on a match
 *    0 on a mismatch
 *   -1 on a syntax error in the pattern
 * A syntactically invalid pattern can generate a mismatch and return 0, so
 * always check the pattern with the patcheck() function.
 * We support both [^abc] and POSIX [!abc] negated character classes
 */
static int patmatch(const char *buf, const char *pat)
{
   int match = 1,m,n;
   while(*pat && match)
     {
	switch(*pat)
	  {
	   case '[':
	     pat++;
	     if(*pat != '^' && *pat != '!')
	       {
		  n = 1;
		  match = 0;
	       }
	     else
	       {
		  pat++;
		  n = 0;
	       }
	     while(*pat != ']')
	       {
		  if(*pat == '\\') pat++;
		  if(!*pat) return -1;
		  if(pat[1] == '-')
		    {
		       m = *pat;
		       pat += 2;
		       if(*pat == '\\')
			 pat++;
		       if(*buf >= m && *buf <= *pat)
			 match = n;
		       if(!*pat)
			 return -1;
		    }
		  else if(*buf == *pat) match = n;
		  pat++;
	       }
	     buf++;
	     break;
	   case '*':
	     pat++;
	     if(!*pat) return 1;
	     while(*buf && !(match = patmatch(buf++,pat)));
	     return match;
	   case '?':
	     if(!*buf) return 0;
	     buf++;
	     break;
	   case '\\':
	     if(*pat)
	       pat++;
	   default:
	     match = (*buf++ == *pat);
	     break;
	  }
	pat++;
	if(match<1) return match;
     }
   if(!*buf) return match;
   return 0;
}


static void slwildcard_match (void)
{
   int rtn;
   char* arg2;

   if (SLang_Num_Function_Args != 2) {
	SLang_verror (SL_USAGE_ERROR,
	   "Usage: Int or Array = wildcard_match(String or Array string,String pattern);");
	return;
   }
   if (SLang_pop_slstring(&arg2))
     {
	SLang_verror (SL_INTRINSIC_ERROR,
		      "Unable to validate arguments to: wildcard_match");
	return;
     }
   
   if (SLang_peek_at_stack () == SLANG_ARRAY_TYPE)
     {
	SLang_Array_Type *at, *out;
	int i;
	char **elementp;
	
	if (-1 == SLang_pop_array_of_type(&at, SLANG_STRING_TYPE))
	  {
	     SLang_verror (SL_INTRINSIC_ERROR,
			   "Unable to validate arguments to: wildcard_match");
	     return;
	  }
	
	if (-1 == patcheck(arg2))
	  {
	     SLang_free_array (at);
	     SLang_verror(SL_INTRINSIC_ERROR, "syntax error in wildcard pattern");
	     SLang_push_null ();
	     return;
	  }
	out = SLang_create_array (SLANG_INT_TYPE, 0, NULL, (int *) &(at->num_elements), 1);
	if (out == NULL)
	  {
	     SLang_free_array (at);
	     SLang_push_null();
	     return;
	  }
	elementp = (char **)at->data;
	for (i=0;i<at->num_elements;i++)
	  {
	     rtn = patmatch(*elementp,arg2);
	     if (rtn == -1)  /* this shouldn't happen */
	       {
		  SLang_free_array (at);
		  SLang_free_array (out);
		  SLang_verror(SL_INTRINSIC_ERROR, "syntax error in wildcard pattern");
		  SLang_push_null ();
		  return;
	       }
	     if (-1 == SLang_set_array_element (out, &i, &rtn))
	       {
		  SLang_free_array (at);
		  SLang_free_array (out);
		  SLang_push_null ();
		  return;
	       }
	     elementp++;
	  }
	(void) SLang_push_array (out, 1);
	SLang_free_array (at);
     }
   else
     {
	char* arg1;
	if (SLang_pop_slstring(&arg1))
	  {
	     SLang_verror (SL_INTRINSIC_ERROR,
			   "Unable to validate arguments to: wildcard_match");
	     return;
	  }
	rtn = patcheck(arg2);
	if (rtn != -1)
	  rtn = patmatch(arg1,arg2);
	SLang_free_slstring(arg1);
	if (rtn == -1)
	  SLang_verror(SL_INTRINSIC_ERROR, "syntax error in wildcard pattern");
	SLang_push_integer(rtn);
     }
}

static SLang_Intrin_Fun_Type Module_Intrinsics [] =
{
   MAKE_INTRINSIC_0("wildcard_match", slwildcard_match, SLANG_VOID_TYPE),
   SLANG_END_INTRIN_FUN_TABLE
};

static SLang_Intrin_Var_Type Module_Variables [] =
{
   MAKE_VARIABLE("_wildcard_module_version_string", &Module_Version_String, SLANG_STRING_TYPE, 1),
   SLANG_END_INTRIN_VAR_TABLE
};

static SLang_IConstant_Type Module_Constants [] =
{
   MAKE_ICONSTANT("_wildcard_module_version", MODULE_VERSION_NUMBER),
   SLANG_END_ICONST_TABLE
};

int init_wildcard_module_ns (char *ns_name)
{
   SLang_NameSpace_Type *ns = SLns_create_namespace (ns_name);
   if (ns == NULL)
     return -1;

   if ((-1 == SLns_add_intrin_var_table (ns, Module_Variables, NULL))
       || (-1 == SLns_add_intrin_fun_table (ns, Module_Intrinsics, NULL))
       || (-1 == SLns_add_iconstant_table (ns, Module_Constants, NULL)))
     return -1;

   return 0;
}
